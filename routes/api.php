<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CalendlysController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::controller(CalendlysController::class)->group(function () {
    Route::get('getCurrentUser','getCurrentUser'); 
    Route::get('activityLogEntries','activityLogEntries'); 
    Route::get('userBusyTimes','userBusyTimes');
    Route::get('userAvailabilitySchedules','userAvailabilitySchedules'); 
    Route::post('dataCompliance','dataCompliance'); 
    Route::get('UserEventTypes','UserEventTypes'); 
    Route::get('eventTypes','eventTypes'); 
    Route::get('eventTypeAvailableTimes','eventTypeAvailableTimes'); 
    Route::get('listOrganizationInvitations','listOrganizationInvitations'); 
    Route::get('inviteUserOrganization','inviteUserOrganization'); 
    Route::delete('revokeUserOrganizationInvitation','revokeUserOrganizationInvitation'); 
    Route::get('getOrganizationInvitation','getOrganizationInvitation'); 
    Route::get('getOrganizationMemberships','getOrganizationMemberships'); 
    Route::delete('removeUserOrganization','removeUserOrganization'); 
    Route::get('listOrganizationMemberships','listOrganizationMemberships'); 
    Route::get('listRoutingForms','listRoutingForms'); 
    Route::get('getRoutingForm','getRoutingForm'); 
    Route::get('listRoutingFormSubmissions','listRoutingFormSubmissions'); 
    Route::get('getRoutingFormSubmissions','getRoutingFormSubmissions'); 
    Route::get('listEventInvitees','listEventInvitees'); 
    Route::get('listEvents','listEvents'); 
    Route::get('getEventInvitee','getEventInvitee'); 
    Route::get('getEvent','getEvent'); 
    Route::get('getInviteeNoShow','getInviteeNoShow'); 
    Route::delete('deleteEvent','deleteEvent'); 
    Route::post('createInviteeNoShow','createInviteeNoShow'); 
    Route::post('cancelEvent','cancelEvent'); 
    Route::post('createSingleUseSchedulingLink','createSingleUseSchedulingLink');
    Route::get('getUser','getUser'); 
    Route::post('createWebhookSubscription','createWebhookSubscription'); 
    Route::get('listWebhookSubscriptions','listWebhookSubscriptions'); 
    Route::get('getWebhookSubscription','getWebhookSubscription'); 
    Route::delete('deleteWebhookSubscription','deleteWebhookSubscription'); 


 



     
});
