<?php

namespace App\Http\Controllers;
use GuzzleHttp\Client;
use Http;
use Illuminate\Http\Request;

class CalendlysController extends Controller
{

    private $request_calendly;
    private $base_url;
    private $headers;


    public function  __construct(Client $request_calendly){

        $this->request_calendly = $request_calendly;
        $this->base_url = env('CALENDY_BASE_URL');
        $this->headers = [
            "Content-Type" => "application/json",
            "Authorization"=> "Bearer" . env('CALENDY_TOKEN')
        ];

    }
    /**
     * Register services.
     *
     * @return void
     */
    public function getCurrentUser(Request $request)
    {

        $response =  Http::withHeaders([
            "Authorization"=> "Bearer " . env('CALENDY_TOKEN'),
            'Content-Type' => 'application/json' 
       ])->get($this->base_url.'/users/me')->json();
       return $response;
    }


    public function activityLogEntries(Request $request)
    {
       

        $response =  Http::withHeaders([
            "Authorization"=> "Bearer " . env('CALENDY_TOKEN'),
            'Content-Type' => 'application/json' 
       ])->get($this->base_url.'/activity_log_entries', [
        'organization' => $request->get('organization'),
        'action' => $request->get('action'),
        'actor' => $request->get('actor'),
        'count' => $request->get('count')
        ])->json();
       return $response;
    }
    


    public function userBusyTimes(Request $request)
    {

        $response =  Http::withHeaders([
            "Authorization"=> "Bearer " . env('CALENDY_TOKEN'),
            'Content-Type' => 'application/json' 
       ])->get($this->base_url.'/user_busy_times', [
        'end_time' => $request->get('end_time'),
        'start_time' => $request->get('start_time'),
        'user' => $request->get('user')
    ])->json();
       return $response;
    }


    public function listUsersAvailabilitySchedules(Request $request)
    {
        
        $response =  Http::withHeaders([
            "Authorization"=> "Bearer " . env('CALENDY_TOKEN'),
            'Content-Type' => 'application/json' 
       ])->get($this->base_url.'/user_availability_schedules', [
        'uuid' => $request->get('uuid')
    ])->json();
       return $response;
    }


    public function userAvailabilitySchedules(Request $request)
    {
        $response =  Http::withHeaders([
            "Authorization"=> "Bearer " . env('CALENDY_TOKEN'),
            'Content-Type' => 'application/json' 
       ])->get($this->base_url.'/user_availability_schedules/'.$request->get('uuid'))->json();
       return $response;
    }

    public function dataCompliance(Request $request)
    {

        $response =  Http::withHeaders([
            "Authorization"=> "Bearer " . env('CALENDY_TOKEN'),
            'Content-Type' => 'application/json' 
       ])->POST($this->base_url.'/data_compliance/deletion/invitees', [
        'emails' => $request->get('emails')
    ])->json();
       return $response;
    }


    public function UserEventTypes(Request $request)
    {

        $response =  Http::withHeaders([
            "Authorization"=> "Bearer " . env('CALENDY_TOKEN'),
            'Content-Type' => 'application/json' 
       ])->get($this->base_url.'/event_types', [
        'organization' => $request->get('organization'),
        'action' => $request->get('action'),
        'actor' => $request->get('actor'),
        'page_token' => $request->get('page_token')
    ])->json();
       return $response;
    }


    public function eventTypes(Request $request)
    {
        $response =  Http::withHeaders([
            "Authorization"=> "Bearer " . env('CALENDY_TOKEN'),
            'Content-Type' => 'application/json' 
       ])->get($this->base_url.'/event_types/'.$request->get('uuid'))->json();
       return $response;
    }


    public function eventTypeAvailableTimes(Request $request)
    {

        $response =  Http::withHeaders([
            "Authorization"=> "Bearer " . env('CALENDY_TOKEN'),
            'Content-Type' => 'application/json' 
       ])->get($this->base_url.'/event_type_available_times', [
        'end_time' => $request->get('end_time'),
        'start_time' => $request->get('start_time'),
        'event_type' => $request->get('event_type')
    ])->json();
       return $response;
    }


    public function listOrganizationInvitations(Request $request)
    {

        $response =  Http::withHeaders([
            "Authorization"=> "Bearer " . env('CALENDY_TOKEN'),
            'Content-Type' => 'application/json' 
       ])->get($this->base_url.'/organizations/'.$request->get('uuid').'/invitations', [
        'count' => $request->get('count'),
        'email' => $request->get('email'),
        'page_token' => $request->get('page_token'),
        'uuid' => $request->get('uuid')
    ])->json();
       return $response;
    }


    public function inviteUserOrganization(Request $request)
    {

        $response =  Http::withHeaders([
            "Authorization"=> "Bearer " . env('CALENDY_TOKEN'),
            'Content-Type' => 'application/json' 
       ])->POST($this->base_url.'/organizations/'.$request->get('uuid').'/invitations', [
        'email' => $request->get('email')
    ])->json();
       return $response;
    }


    public function revokeUserOrganizationInvitation(Request $request)
    {

        $response =  Http::withHeaders([
            "Authorization"=> "Bearer " . env('CALENDY_TOKEN'),
            'Content-Type' => 'application/json' 
       ])->delete($this->base_url.'/organizations/'.$request->get('org_uuid').'/invitations/'.$request->get('uuid'), [
        'org_uuid' => $request->get('org_uuid'),
        'uuid' => $request->get('uuid')
    ])->json();
       return $response;
    }

    public function getOrganizationInvitation(Request $request)
    {

        $response =  Http::withHeaders([
            "Authorization"=> "Bearer " . env('CALENDY_TOKEN'),
            'Content-Type' => 'application/json' 
       ])->get($this->base_url.'/organizations/'.$request->get('org_uuid').'/invitations/'.$request->get('uuid'), [
        'org_uuid' => $request->get('org_uuid'),
        'uuid' => $request->get('uuid')
    ])->json();
       return $response;
    }


    public function getOrganizationMemberships(Request $request)
    {

        $response =  Http::withHeaders([
            "Authorization"=> "Bearer " . env('CALENDY_TOKEN'),
            'Content-Type' => 'application/json' 
       ])->get($this->base_url.'/organization_memberships/'.$request->get('uuid'), [
        'uuid' => $request->get('uuid')
    ])->json();
       return $response;
    }


    public function removeUserOrganization(Request $request)
    {

        $response =  Http::withHeaders([
            "Authorization"=> "Bearer " . env('CALENDY_TOKEN'),
            'Content-Type' => 'application/json' 
        ])->delete($this->base_url.'/organization_memberships/'.$request->get('uuid'), [
                'org_uuid' => $request->get('org_uuid'),
        'uuid' => $request->get('uuid')
    ])->json();
       return $response;
    }


    public function listOrganizationMemberships(Request $request)
    {

        $response =  Http::withHeaders([
            "Authorization"=> "Bearer " . env('CALENDY_TOKEN'),
            'Content-Type' => 'application/json' 
       ])->get($this->base_url.'/organization_memberships', [
        'count' => $request->get('count'),
        'email' => $request->get('email'),
        'page_token' => $request->get('page_token'),
        'organization' => $request->get('organization')
    ])->json();
       return $response;
    }


    public function listRoutingForms(Request $request)
    {

        $response =  Http::withHeaders([
            "Authorization"=> "Bearer " . env('CALENDY_TOKEN'),
            'Content-Type' => 'application/json' 
       ])->get($this->base_url.'/routing_forms', [
        'count' => $request->get('count'),
        'sort' => $request->get('sort'),
        'page_token' => $request->get('page_token'),
        'organization' => $request->get('organization')
    ])->json();
       return $response;
    }


    public function getRoutingForm(Request $request)
    {

        $response =  Http::withHeaders([
            "Authorization"=> "Bearer " . env('CALENDY_TOKEN'),
            'Content-Type' => 'application/json' 
       ])->get($this->base_url.'/routing_forms/'.$request->get('uuid'), [
        'uuid' => $request->get('uuid')
    ])->json();
       return $response;
    }


    public function listRoutingFormSubmissions(Request $request)
    {

        $response =  Http::withHeaders([
            "Authorization"=> "Bearer " . env('CALENDY_TOKEN'),
            'Content-Type' => 'application/json' 
       ])->get($this->base_url.'/routing_form_submissions', [
        'count' => $request->get('count'),
        'form' => $request->get('form'),
        'page_token' => $request->get('page_token'),
        'sort' => $request->get('sort')
    ])->json();
       return $response;
    }


    public function getRoutingFormSubmissions(Request $request)
    {

        $response =  Http::withHeaders([
            "Authorization"=> "Bearer " . env('CALENDY_TOKEN'),
            'Content-Type' => 'application/json' 
       ])->get($this->base_url.'/routing_form_submissions/'.$request->get('uuid'), [
        'uuid' => $request->get('uuid')
    ])->json();
       return $response;
    }




    public function listEventInvitees(Request $request)
    {

        $response =  Http::withHeaders([
            "Authorization"=> "Bearer " . env('CALENDY_TOKEN'),
            'Content-Type' => 'application/json' 
       ])->get($this->base_url.'/scheduled_events/'.$request->get('uuid').'/invitees', [
        'count' => $request->get('count'),
        'email' => $request->get('email'),
        'page_token' => $request->get('page_token'),
        'uuid' => $request->get('uuid')
    ])->json();
       return $response;
    }


    public function listEvents(Request $request)
    {

        $response =  Http::withHeaders([
            "Authorization"=> "Bearer " . env('CALENDY_TOKEN'),
            'Content-Type' => 'application/json' 
       ])->get($this->base_url.'/organizations/'.$request->get('uuid').'/invitations', [
        'count' => $request->get('count'),
        'invitee_email' => $request->get('invitee_email'),
        'max_start_time' => $request->get('max_start_time'),
        'min_start_time' => $request->get('min_start_time')
    ])->json();
       return $response;
    }


    public function getEventInvitee(Request $request)
    {

        $response =  Http::withHeaders([
            "Authorization"=> "Bearer " . env('CALENDY_TOKEN'),
            'Content-Type' => 'application/json' 
       ])->get($this->base_url.'/scheduled_events/'.$request->get('event_uuid').'/invitees/'.$request->get('invitee_uuid'), [
        'event_uuid' => $request->get('event_uuid'),
        'invitee_uuid' => $request->get('invitee_uuid')
    ])->json();
       return $response;
    }

    public function getEvent(Request $request)
    {

        $response =  Http::withHeaders([
            "Authorization"=> "Bearer " . env('CALENDY_TOKEN'),
            'Content-Type' => 'application/json' 
       ])->get($this->base_url.'/scheduled_events/'.$request->get('uuid'), [
        'uuid' => $request->get('uuid')
    ])->json();
       return $response;
    }


    public function getInviteeNoShow(Request $request)
    {

        $response =  Http::withHeaders([
            "Authorization"=> "Bearer " . env('CALENDY_TOKEN'),
            'Content-Type' => 'application/json' 
       ])->get($this->base_url.'/invitee_no_shows/'.$request->get('uuid'), [
        'uuid' => $request->get('uuid')
    ])->json();
       return $response;
    }


    public function deleteEvent(Request $request)
    {

        $response =  Http::withHeaders([
            "Authorization"=> "Bearer " . env('CALENDY_TOKEN'),
            'Content-Type' => 'application/json' 
       ])->get($this->base_url.'/invitee_no_shows/'.$request->get('uuid'), [
        'uuid' => $request->get('uuid')
    ])->json();
       return $response;
    }


    public function createInviteeNoShow(Request $request)
    {

        $response =  Http::withHeaders([
            "Authorization"=> "Bearer " . env('CALENDY_TOKEN'),
            'Content-Type' => 'application/json' 
       ])->POST($this->base_url.'/invitee_no_shows', [
        'invitee' => $request->get('invitee')
    ])->json();
       return $response;
    }

    public function cancelEvent(Request $request)
    {

        $response =  Http::withHeaders([
            "Authorization"=> "Bearer " . env('CALENDY_TOKEN'),
            'Content-Type' => 'application/json' 
       ])->POST($this->base_url.'/scheduled_events/'.$request->get('uuid').'/cancellation', [
        'uuid' => $request->get('uuid')
    ])->json();
       return $response;
    }


    public function createSingleUseSchedulingLink(Request $request)
    {

        $response =  Http::withHeaders([
            "Authorization"=> "Bearer " . env('CALENDY_TOKEN'),
            'Content-Type' => 'application/json' 
       ])->POST($this->base_url.'/scheduling_links', [
        'max_event_count' => $request->get('max_event_count'),
        'owner' => $request->get('owner'),
        'owner_type' => $request->get('owner_type')
    ])->json();
       return $response;
    }



    public function getUser(Request $request)
    {

        $response =  Http::withHeaders([
            "Authorization"=> "Bearer " . env('CALENDY_TOKEN'),
            'Content-Type' => 'application/json' 
       ])->get($this->base_url.'/users/'.$request->get('uuid'), [
        'uuid' => $request->get('uuid')
    ])->json();
       return $response;
    }



    public function createWebhookSubscription(Request $request)
    {

        $response =  Http::withHeaders([
            "Authorization"=> "Bearer " . env('CALENDY_TOKEN'),
            'Content-Type' => 'application/json' 
       ])->post($this->base_url.'/webhook_subscriptions', [
        'url' => $request->get('url'),
        'events' => $request->get('events'),
        'organization' => $request->get('organization'),
        'user' => $request->get('user'),
        'scope' => $request->get('scope'),
        'signing_key' => $request->get('signing_key')
    ])->json();
       return $response;
    }




    public function listWebhookSubscriptions(Request $request)
    {

        $response =  Http::withHeaders([
            "Authorization"=> "Bearer " . env('CALENDY_TOKEN'),
            'Content-Type' => 'application/json' 
       ])->get($this->base_url.'/webhook_subscriptions', [
        'organization' => $request->get('organization'),
        'count' => $request->get('count'),
        'scope' => $request->get('scope'),
        'page_token' => $request->get('page_token')
    ])->json();
       return $response;
    }


    public function getWebhookSubscription(Request $request)
    {

        $response =  Http::withHeaders([
            "Authorization"=> "Bearer " . env('CALENDY_TOKEN'),
            'Content-Type' => 'application/json' 
       ])->get($this->base_url.'/webhook_subscriptions/'.$request->get('webhook_uuid'), [
        'webhook_uuid' => $request->get('webhook_uuid')
    ])->json();
       return $response;
    }


    public function deleteWebhookSubscription(Request $request)
    {

        $response =  Http::withHeaders([
            "Authorization"=> "Bearer " . env('CALENDY_TOKEN'),
            'Content-Type' => 'application/json' 
       ])->delete($this->base_url.'/webhook_subscriptions/'.$request->get('webhook_uuid'), [
        'webhook_uuid' => $request->get('webhook_uuid')
    ])->json();
       return $response;
    }

    










    
}
